const DSCV_LOCALSTORAGE = "DSCV_LOCALSTORAGE";

var dscv = [];

var dscvHoanThanh = []

var dscvJson = localStorage.getItem("DSCV_LOCALSTORAGE");

if (dscvJson != null) {
    dscv = JSON.parse(dscvJson);
    for (var index = 0; index < dscv.length; index++) {
        var cv = dscv[index];
        dscv[index] = new CongViec(
            cv.tenCv,
        );
    }

    renderDSCV(dscv);
}

document.getElementById("addItem").addEventListener('click', function () {

    var newCV = layThongTinTuForm();
    let isValid = validator.kiemTraRong(
        newCV.tenCv,
        "tbTask",
        "Chưa nhập nhiệm vụ!"
    );

    if (isValid) {
        dscv.push(newCV);

        var dscvJson = JSON.stringify(dscv);
        localStorage.setItem("DSCV_LOCALSTORAGE", dscvJson);
        renderDSCV(dscv);
        1;
    }

})

function xoaCongViec(id) {
    // console.log(id);

    var index = timKiemViTri(id, dscv);
    if (index != -1) {
        dscv.splice(index, 1);

        renderDSCV(dscv);
    }
    console.log('index: ', index);
}

function danhDauDaLam(id) {
    var index = timKiemViTri(id, dscv);
    if (index != -1) {
        var currentTask = dscv[index];
        dscvHoanThanh.push(currentTask);
        if (index != 1) {
            renderDSCV(dscv);
        }
        showDaHoanThanh(dscvHoanThanh);
        1;
    }
}

function xoaViecDaLamXong(ten) {
    var index = timKiemViTri2(ten, dscvHoanThanh);
    if (index != -1) {
        dscvHoanThanh.splice(index, 1);
        showDaHoanThanh(dscvHoanThanh);
    }
}

// start descending sort
document.getElementById("three").onclick = function () {
    // sort is used to sort the elements
    dscv.sort(function (a, b) {
        return b.tenCv.localeCompare(a.tenCv);
    });
    renderDSCV(dscv);
};
// end descending sort

// start Sort up
document.getElementById("two").onclick = function () {
    // console.log("yes");
    // sort is used to sort the elements
    dscv.sort(function (a, b) {
        return a.tenCv.localeCompare(b.tenCv);
    });
    renderDSCV(dscv);
};